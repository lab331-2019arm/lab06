import { Component } from '@angular/core';
import { map, shareReplay } from 'rxjs/operators';
import Student from '../entity/student';
import { StudentService } from '../service/student-service';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-my-nav',
  templateUrl: './my-nav.component.html',
  styleUrls: ['./my-nav.component.css']
})
export class MyNavComponent {

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );
    students$: Observable<Student[]> = this.studentService.getStudents();
  
    constructor(private breakpointObserver: BreakpointObserver,
       private studentService: StudentService) {}
  
}
